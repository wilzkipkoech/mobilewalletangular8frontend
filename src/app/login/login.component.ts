import { Component, OnInit } from '@angular/core';
import { LoginDataService } from '../service/login-data.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginerror = false;
  loginErrorMessage: any;
  loginResponse: any;
  loginsuccessMessge: any;
  loginSuccess = false;
  constructor(private loginconnect: LoginDataService) {
  }

  ngOnInit() {
  }


  login(username, password) {
    const loginDetails = JSON.stringify({ customerId: username, pin: password});
    this.loginResponse = this.loginconnect.CustomerLogin(loginDetails)
      .subscribe((data) => {
        this.loginResponse = data;
        if (this.loginResponse.hasOwnProperty('status')) {
          this.loginerror = true;
          this.loginSuccess = false;
          this.loginErrorMessage = this.loginResponse.error;
        } else {
          // tslint:disable-next-line: max-line-length
          this.loginsuccessMessge = `Welcome ${this.loginResponse.firstName} ${this.loginResponse.lastName} to M-wallet Backend portal`;
          this.loginSuccess = true;
          this.loginerror = false;
        }
      });
  }

}
