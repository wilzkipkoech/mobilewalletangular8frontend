import { Component, OnInit } from '@angular/core';
import { CustomerDataService } from '../service/customer-data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';



export class Customer {
  constructor(
    public id: number,
    public first_name: string,
    public last_name: string,
    public email: string,
     public customer_id: string,
    public  account_no : string,
    public balance: number
    ) {

  }
}
@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})



export class CustomersComponent implements OnInit {

  customers: Customer[]
  customerById :Customer[]


  constructor(private service: CustomerDataService,
   private router: Router,
    private toastr: ToastrService) { }

    ngOnInit() {
      this.retrieveAllCustomers();
    }

    retrieveAllCustomers() {
      this.service.retrieveAllCustomers().subscribe(
        response => {
           console.log(response);
          this.customers = response;
        }
      );
    }



    GetCustomerAccountByCustomerById(CustomerId: any) {
      this.service.GetTransactionByCustomerId(CustomerId)
        .subscribe(
          response => {
            console.log(response);
           this.customerById = response;
         }
       );
    }
}
