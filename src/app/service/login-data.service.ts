import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';




@Injectable({
  providedIn: 'root'
})


export class LoginDataService {

  constructor(private http: HttpClient) { }



  CustomerLogin(body: any) {

    return this.http.post('http://localhost:8092/api/v1/webusers/login', body);
  }
}


