import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Customer } from 'src/app/customers/customers.component';
import { API_URL } from 'src/app/app.constants';


@Injectable({
  providedIn: 'root'
})
export class CustomerDataService {

  constructor(private http: HttpClient) { }

  retrieveAllCustomers() {
    return this.http.get<Customer[]>(`${API_URL}/api/v1/customers/details`);
  }

  GetTransactionByCustomerId(CustomerId: any) {
    return this.http.get<Customer[]>(`${API_URL}/api/v1/customers/details/`+ CustomerId);
}

}
