import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Balance } from 'src/app/account-balance/account-balance.component';
import { API_URL } from 'src/app/app.constants';


@Injectable({
  providedIn: 'root'
})
export class BalanceDataService {

  constructor(private http: HttpClient) { }

  retrieveAccountBalance() {
    return this.http.get<Balance[]>(`${API_URL}/api/v1/accounts/`);
  }

  getAccountBalance(accountNo: any) {
    return this.http.get<Balance[]>(`${API_URL}/api/v1/accounts/`+ accountNo);
}



}
