import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Transaction } from 'src/app/transactions/transactions.component';
import { API_URL } from 'src/app/app.constants';


@Injectable({
  providedIn: 'root'
})
export class TransactionDataService {

  constructor(private http: HttpClient) { }

  retrieveAllTrasactions() {
    return this.http.get<Transaction[]>(`${API_URL}/api/v1/transactions/`);
  }


  GetTransactionByCustomerId(CustomerId: any) {
      return this.http.get<Transaction[]>(`${API_URL}/api/v1/transactions/`+ CustomerId);
  }

}
