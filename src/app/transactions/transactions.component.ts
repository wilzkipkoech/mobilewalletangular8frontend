import { Component, OnInit } from "@angular/core";

import { TransactionDataService } from '../service/transactions-data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


export class Transaction {
  constructor(
    public  transaction_id: string,
    public customer_id: string,
    public  account_no : string,
    public  amount : number,
    public  transaction_type : string,
    public  debit_credit : string,
    public balance: number) {
  }
}
@Component({
  selector: "app-transactions",
  templateUrl: "./transactions.component.html",
  styleUrls: ["./transactions.component.scss"],
})
export class TransactionsComponent implements OnInit {
  transactions: Transaction[]

  transactionsById: Transaction[]



  constructor(private service:  TransactionDataService ,
   private router: Router,
    private toastr: ToastrService) { }

    ngOnInit() {
      this.retrieveAllTrasactions();
    }

    retrieveAllTrasactions() {
      this.service.retrieveAllTrasactions().subscribe(
        response => {
           console.log(response);
          this.transactions = response;
        }
      );
    }

    seachTransactionByTransactionId( CustomerId: any) {
      this.service.GetTransactionByCustomerId(CustomerId)
        .subscribe(
          response => {
            console.log(response);
           this.transactionsById = response;
         }
       );
    }
}
